
# **Connectify**

## Overview

Connectify is a RESTful API service built with FastAPI, designed as a simple communication platform similar to 
Microsoft Teams.
The project uses **MongoDB** as its database for managing users, groups, and messages, with a scalable architecture 
based on **CQRS** (Command Query Responsibility Segregation) for clear separation of responsibilities.

## Architecture

This project adopts a modular structure, following best practices in FastAPI and CQRS architecture. Key features include:

- **CQRS Pattern**: This architecture separates command (write) and query (read) operations, which helps scale the application efficiently and makes the codebase maintainable.
- **MongoDB**: A NoSQL database used to store all user, group, and message data, offering flexibility and high scalability.
- **FastAPI**: Provides a lightweight and efficient framework for building APIs with asynchronous request handling, ideal for real-time applications.

## Prerequisites

- **Python**: Version 3.12+
- **Docker** and **Docker Compose**: Required to run the application in a containerized environment.
- **Poetry**: This project uses Poetry to manage dependencies (defined in `pyproject.toml`).

## Running the Project

The project is containerized with Docker to simplify deployment and ensure a consistent environment.

### Quick Start with Docker

1. **Clone the repository**:
   ```bash
   git clone <repository-url>
   cd connectify
   ```

2. **Configure environment variables**:
   - Create a `.env` file in the root directory base on `env.example`:

3. **Build the Docker image**:
   ```bash
   docker-compose build
   ```

4. **Start the application**:
   ```bash
   docker-compose up
   ```

   By default, the application will be available at `http://localhost:8000`. 
The MongoDB service will be accessible at `localhost:27017`.

5. **Access API Documentation**:
   - FastAPI automatically generates interactive API documentation. Once the application is running, visit:
     - Swagger UI: [http://localhost:8000/docs](http://localhost:8000/docs)
     - Redoc: [http://localhost:8000/redoc](http://localhost:8000/redoc)

### Running Tests

To run tests, use the following command inside the Docker container or local environment:

```bash
pytest
```

## License

This project is licensed under the MIT License.

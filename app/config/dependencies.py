from typing import Annotated

from fastapi import Depends

from app.db.models.auth import CurrentLoginUser
from app.service.auth_service import get_current_user

user_dependency = Annotated[CurrentLoginUser, Depends(get_current_user)]

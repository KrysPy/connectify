from mediatr import Mediator

from app.handlers.commands.group.create_group import CreateGroupHandler
from app.handlers.commands.message.create_message import CreateMessageHandler
from app.handlers.commands.group.delete_group import DeleteGroupHandler
from app.handlers.commands.group.edit_group import UpdateGroupHandler
from app.handlers.commands.user.create_user import CreateUserHandler
from app.handlers.queries.get_group import GetAllUserGroupsHandler, GetGroupByIdHandler
from app.handlers.queries.get_message import GetMessagesForGroupHandler
from app.handlers.queries.get_user import GetUserHandler
from app.handlers.queries.login import LoginUserHandler


def _configurate_mediator(_mediator: Mediator) -> None:
    """
    Register the necessary command handlers with the mediator.

    :param _mediator: The mediator instance used for managing commands and handlers.
    :type _mediator: Mediator
    """
    # commands
    _mediator.register_handler(CreateUserHandler)
    _mediator.register_handler(CreateGroupHandler)
    _mediator.register_handler(UpdateGroupHandler)
    _mediator.register_handler(DeleteGroupHandler)
    _mediator.register_handler(CreateMessageHandler)

    # queries
    _mediator.register_handler(LoginUserHandler)
    _mediator.register_handler(GetUserHandler)
    _mediator.register_handler(GetAllUserGroupsHandler)
    _mediator.register_handler(GetGroupByIdHandler)
    _mediator.register_handler(GetMessagesForGroupHandler)


mediator = Mediator()
_configurate_mediator(mediator)

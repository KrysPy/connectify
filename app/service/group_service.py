from bson import json_util, ObjectId
import json

from app.db.db import MongoCollections, get_database
from app.db.models.group import Group
from app.exceptions.group import (
    GroupAlreadyExists,
    GroupNotFoundError,
    PermissionDeniedError,
)


class GroupService:
    def __init__(self):
        self.db = get_database()

    async def create_group(
        self, name: str, owner_id: str, is_private: bool
    ) -> dict[str, str]:
        """
        Create a new group in the database with a specified owner and privacy setting.

        :param name: The name of the group to create.
        :param owner_id: The unique identifier of the user who owns the group.
        :param is_private: Indicates whether the group is private or public.

        :returns: A dictionary containing the unique identifier of the created group.
        """
        group = Group(
            name=name,
            owner_id=owner_id,
            members=[ObjectId(owner_id)],
            is_private=is_private,
        )
        group_dict = group.model_dump(by_alias=True, exclude={"id"})

        all_user_groups = await self.get_all_user_groups(owner_id)

        if any(group["name"] == name for group in all_user_groups):
            raise GroupAlreadyExists

        result = await self.db.groups.insert_one(group_dict)

        return {"group_id": str(result.inserted_id)}

    async def get_all_user_groups(self, user_id: str) -> list[dict]:
        """
        Retrieve all groups associated with a specific user.

        :param user_id: The unique identifier of the user whose groups are to be retrieved.

        :returns: A list of dictionaries, each representing a group owned by the user.
        """
        groups = (
            await self.db[MongoCollections.GROUPS.value]
            .find({"owner_id": user_id})
            .to_list(length=None)
        )
        return json.loads(json_util.dumps(groups))

    async def get_group_by_id(self, group_id: str, owner_id: str) -> dict:
        """
        Retrieve a group from the database using its unique identifier.

        :param group_id: The unique identifier of the group to retrieve.
        :param owner_id: The unique identifier of the group owner.


        :returns: A dictionary containing details of the specified group.
        """
        group = await self.db[MongoCollections.GROUPS.value].find_one(
            {"_id": ObjectId(group_id)}
        )

        if not group:
            raise GroupNotFoundError

        if group["is_private"]:
            await self._check_is_owner(group_id, owner_id)

        return json.loads(json_util.dumps(group))

    async def update_group(
        self, group_id: str, owner_id: str, name: str = None, is_private: bool = None
    ) -> dict:
        """
        Update the specified fields of a group in the database.

        :param group_id: The unique identifier of the group to update.
        :param owner_id: The unique identifier of the group owner.
        :param name: The new name for the group, if provided.
        :param is_private: The new privacy setting for the group, if provided.

        :returns: A dictionary with the updated group details.
        """
        await self._check_is_owner(group_id, owner_id)

        update_data = {}

        if name is not None:
            update_data["name"] = name
        if is_private is not None:
            update_data["is_private"] = is_private

        group = await self.db[MongoCollections.GROUPS.value].update_one(
            {"_id": ObjectId(group_id)}, {"$set": update_data}
        )

        if group.matched_count == 0:
            raise GroupNotFoundError

        updated_group = await self.db[MongoCollections.GROUPS.value].find_one(
            {"_id": ObjectId(group_id)}
        )
        return json.loads(json_util.dumps(updated_group))

    async def delete_group(self, group_id: str, owner_id: str) -> dict:
        """
        Delete a group from the database by its unique identifier.

        :param group_id: The unique identifier of the group to delete.
        :param owner_id: The unique identifier of the group owner

        :returns: A dictionary indicating success and the ID of the deleted group.
        """
        await self._check_is_owner(group_id, owner_id)

        result = await self.db[MongoCollections.GROUPS.value].delete_one(
            {"_id": ObjectId(group_id)}
        )

        if result.deleted_count == 0:
            raise GroupNotFoundError

        return {"message": "Group deleted successfully", "group_id": group_id}

    async def _check_is_owner(self, group_id: str, user_id: str) -> None:
        """
        Check if the user is the owner of the specified group.

        :param group_id: The unique identifier of the group.
        :param user_id: The unique identifier of the user to check.
        """
        group = await self.db[MongoCollections.GROUPS.value].find_one(
            {"_id": ObjectId(group_id)}
        )

        if not group:
            raise GroupNotFoundError

        if not group["owner_id"] == user_id:
            raise PermissionDeniedError("User is not the owner of the group.")

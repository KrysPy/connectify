from bson import ObjectId, json_util
import json

from app.db.db import MongoCollections, get_database
from app.db.models.user import User
from app.exceptions.auth import EmailAlreadyExists
from app.exceptions.user import UserNotFoundError


class UserService:
    def __init__(self):
        self.db = get_database()

    async def create_user(
        self, username: str, email: str, hashed_password: str, full_name: str
    ) -> dict[str, str]:
        """
        Create a new user object in the MongoDB database.

        :param username: username
        :param email: user email
        :param hashed_password: hashed user password
        :param full_name: user full name

        :return: A dictionary containing the ID of the inserted user.
        """

        user = User(
            username=username,
            email=email,
            password=hashed_password,
            full_name=full_name,
        )
        user_in_db = await self.db[MongoCollections.USERS.value].find_one(
            {"email": email}
        )
        if user_in_db:
            raise EmailAlreadyExists

        user_dict = user.model_dump(by_alias=True, exclude={"id"})
        result = await self.db[MongoCollections.USERS.value].insert_one(user_dict)

        return {"user_id": str(result.inserted_id)}

    async def get_user_by_id(self, user_id: str) -> dict:
        """
        Retrieve a user from the DB by their unique ID.

        :param user_id: The unique identifier of the user to retrieve.

        :returns: A dictionary containing user details if found.
        """

        user = await self.db[MongoCollections.USERS.value].find_one(
            {"_id": ObjectId(user_id)}
        )

        if user:
            return json.loads(json_util.dumps(user))

        raise UserNotFoundError("User with given ID not found")

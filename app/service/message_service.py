from bson import json_util
import json

from app.db.db import MongoCollections, get_database
from app.db.models.message import Message
from app.service.group_service import GroupService


class MessageService:
    def __init__(self):
        self.db = get_database()
        self.group_service = GroupService()

    async def create_message(self, sender_id: str, group_id: str, content: str) -> dict:
        """
        Creates a new message in the specified group.

        :param sender_id: The ID of the user sending the message.
        :param group_id: The ID of the group where the message is being sent.
        :param content: The content of the message.

        :return dict: A dictionary containing the ID of the created message, with the key "message_id".
        """
        group = Message(sender_id=sender_id, group_id=group_id, content=content)
        group_dict = group.model_dump(by_alias=True, exclude={"id"})

        result = await self.db[MongoCollections.MESSAGES.value].insert_one(group_dict)
        return {"message_id": str(result.inserted_id)}

    async def get_message_for_group(self, group_id: str, user_id: str) -> list[dict]:
        """
        Retrieves all messages for a specific group.

        :param group_id: The ID of the group whose messages are being retrieved.
        :param user_id: The ID of the user requesting the messages. Used for access control.

        :return list[dict]: A list of messages as dictionaries, formatted as JSON.
        """
        group = await self.group_service.get_group_by_id(group_id, user_id)

        messages = (
            await self.db[MongoCollections.MESSAGES.value]
            .find({"group_id": group["_id"]["$oid"]})
            .to_list(length=None)
        )

        return json.loads(json_util.dumps(messages))

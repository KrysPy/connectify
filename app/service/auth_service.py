from datetime import timedelta, datetime, timezone
from typing import Annotated, Any, Mapping
import os
import logging

from argon2 import PasswordHasher
from fastapi.security import OAuth2PasswordBearer
from fastapi import Depends
from jwt.exceptions import InvalidTokenError
import jwt

from app.db.db import get_database
from app.db.models.auth import CurrentLoginUser
from app.exceptions.auth import InvalidDataInToken

log = logging.getLogger("uvicorn.error")
oauth2_bearer = OAuth2PasswordBearer(tokenUrl="auth/login")


def _verify_password(plain_password: str, hashed_password: str) -> bool:
    """
    Verify if the provided password matches the stored hashed password.

    :param plain_password: The plain text password provided during login.
    :type plain_password: str
    :param hashed_password: The hashed password retrieved from the database for the user.
    :type hashed_password: str
    :return: True if the password matches the hash, False otherwise.
    :rtype: bool
    """
    ph = PasswordHasher()
    return ph.verify(hashed_password, plain_password)


def create_access_token(data: dict) -> str:
    """
    Create a JWT access token with an optional expiration time.

    :param data: A dictionary containing the data to be encoded in the token (e.g., user information).
    :type data: dict
    :return: The encoded JWT token as a string.
    :rtype: str
    """
    to_encode = data.copy()
    now = datetime.now(timezone.utc)
    access_token_expires = (
        timedelta(minutes=float(os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES"))) + now
    )
    to_encode.update({"exp": access_token_expires})
    encoded_jwt = jwt.encode(
        to_encode, os.getenv("SECRET_KEY"), algorithm=os.getenv("ALGORYTHM")
    )
    return encoded_jwt


async def authenticate_user(email: str, password: str) -> Mapping[str, Any] | bool:
    """
    Authenticate a user by verifying their email and password.

    :param email: The user's email address.
    :type email: str
    :param password: The user's plain text password.
    :type password: str
    :return: True if the user exists and the password is correct, False otherwise.
    :rtype: bool
    """
    db = get_database()
    user = await db["users"].find_one({"email": email})
    if not user:
        return False
    if not _verify_password(password, user["password"]):
        return False
    user["_id"] = str(user["_id"])
    return user


async def get_current_user(
    token: Annotated[str, Depends(oauth2_bearer)]
) -> CurrentLoginUser:
    """
    Verify and decode the JWT token to retrieve the authenticated user's information.

    :param token: JWT token provided in the `Authorization` header in Bearer format.
    :type token: str
    :return: A dictionary containing the user's email and ID if the token is valid.
    :rtype: dict
    :raises HTTPException: If the token is invalid, expired, or does not contain user data.
    :raises UserNotFoundError: If the token is valid but lacks necessary user information.
    """
    try:
        payload = jwt.decode(
            token, os.getenv("SECRET_KEY"), algorithms=[os.getenv("ALGORYTHM")]
        )
        email = payload.get("sub")
        user_id = payload.get("id")
        if email is None or user_id is None:
            raise InvalidDataInToken
        return CurrentLoginUser(email=email, user_id=user_id)
    except InvalidTokenError:
        raise InvalidDataInToken

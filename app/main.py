from dotenv import load_dotenv

load_dotenv(".env")

from contextlib import asynccontextmanager
import logging

from fastapi import FastAPI

from app.api import users, auth, groups, message
from app.db.db import MongoDatabase

log = logging.getLogger("uvicorn.error")


log.info("Environment variables loaded")


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Context manager that manages the application lifespan, opening and closing the database connection.

    :param app: The FastAPI application instance.
    :type app: FastAPI

    :yield: This function is called at the application startup and shutdown.
    """
    db_instance = MongoDatabase()
    await db_instance.ensure_collections_exist()
    log.info("Database initialized with required collections.")
    yield
    db_instance.close()


app = FastAPI(
    title="Connectify",
    description="REST API for simple communicator app",
    lifespan=lifespan,
)

app.include_router(auth.router, prefix="/auth", tags=["auth"])
app.include_router(users.router, prefix="/user", tags=["user"])
app.include_router(groups.router, prefix="/group", tags=["group"])
app.include_router(message.router, prefix="/messages", tags=["message"])

from enum import Enum, unique
import logging
import os

from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase
from pymongo.errors import CollectionInvalid

log = logging.getLogger("uvicorn.error")


@unique
class MongoCollections(Enum):
    """Enum representing MongoDB collection names."""

    USERS: str = "users"
    GROUPS: str = "groups"
    MESSAGES: str = "messages"

    @classmethod
    def get_all_collections(cls) -> list[str]:
        """
        Retrieve all collection names defined in the Enum.

        return list[str]: A list of all collection names as strings.
        """
        return [item.value for item in cls]


class MongoDatabase:
    _instance = None
    _client: AsyncIOMotorClient = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        if self._client is None:
            connection_str = os.getenv("MONGO_DB_CONNECTION_STR")
            if not connection_str:
                raise ValueError("Database connection string not provided.")
            self._client = AsyncIOMotorClient(connection_str)
            log.info("Database connection initialized!")

    def get_database(self) -> AsyncIOMotorDatabase:
        """Retrieve the MongoDB database instance."""
        return self._client.get_database(os.getenv("MONGO_DB_NAME"))

    async def ensure_collections_exist(self) -> None:
        """Ensure that required collections exist in the database."""
        db = self.get_database()
        existing_collections = await db.list_collection_names()

        for collection_name in MongoCollections.get_all_collections():
            if collection_name not in existing_collections:
                try:
                    await db.create_collection(collection_name)
                    log.info(f"Collection: {collection_name} created!")
                except CollectionInvalid:
                    log.error(f"Failed to create collection: {collection_name}")
            else:
                log.info(f"Collection {collection_name} already exists.")

    def close(self) -> None:
        """Close the MongoDB connection."""
        self._client.close()
        log.info("Database connection closed.")


def get_database() -> AsyncIOMotorDatabase:
    """
    Dependency to provide an instance of the MongoDB database.

    This function initializes an instance of `MongoDatabase` and retrieves
    the asynchronous database connection (`AsyncIOMotorDatabase`).

    :return AsyncIOMotorDatabase: An asynchronous connection to the MongoDB database.
    """
    db_instance = MongoDatabase()
    return db_instance.get_database()

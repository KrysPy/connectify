from typing import Optional, List
from datetime import datetime

from pydantic import BaseModel, Field, ConfigDict

from app.config.common_const import PyObjectId


class Group(BaseModel):
    """Group entity"""

    id: Optional[PyObjectId] = Field(alias="_id", default=None)
    name: Optional[str] = None
    owner_id: PyObjectId = None
    members: List[PyObjectId] = Field(...)
    is_private: bool = Field(default=False)
    created_at: Optional[datetime] = Field(default_factory=datetime.now)
    updated_at: Optional[datetime] = None
    deleted_at: Optional[datetime] = None
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        json_schema_extra={
            "example": {
                "name": None,
                "owner_id": "607f1f77bcf86cd799439011",
                "members": ["607f1f77bcf86cd799439011", "607f1f77bcf86cd799439022"],
                "is_private": True,
            }
        },
    )


class UpdateGroupRequestDto(BaseModel):
    name: Optional[str] = None
    is_private: bool = Field(default=False)

from pydantic import BaseModel, EmailStr, Field

from app.config.common_const import PyObjectId


class Token(BaseModel):
    """Auth token model"""

    access_token: str
    token_type: str


class CurrentLoginUser(BaseModel):
    """Currently login user model"""

    email: EmailStr = Field(...)
    user_id: PyObjectId = Field(...)

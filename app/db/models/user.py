from datetime import datetime
from typing import Optional, List
from bson import ObjectId

from pydantic import BaseModel, EmailStr, Field, ConfigDict

from app.config.common_const import PyObjectId


class User(BaseModel):
    """User entity - full user model"""

    id: Optional[PyObjectId] = Field(alias="_id", default=None)
    username: str = Field(...)
    email: EmailStr = Field(...)
    password: str = Field(...)
    full_name: str = Field(...)
    groups: Optional[List[ObjectId]] = []
    created_at: Optional[datetime] = Field(default_factory=datetime.now)
    updated_at: Optional[datetime] = None
    deleted_at: Optional[datetime] = None
    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True,
        json_schema_extra={
            "example": {
                "username": "Johnny123",
                "email": "j.kowalski@example.com",
                "password": "ASDn6&AWD53hj",
                "full_name": "Jan Kowalski",
            }
        },
    )

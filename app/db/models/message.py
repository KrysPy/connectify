from typing import Optional
from datetime import datetime

from pydantic import BaseModel, Field, ConfigDict

from app.config.common_const import PyObjectId


class Message(BaseModel):
    """Message entity - full message model"""

    id: Optional[PyObjectId] = Field(alias="_id", default=None)
    sender_id: PyObjectId = Field(...)
    group_id: PyObjectId = Field(...)
    content: str = Field(...)
    created_at: Optional[datetime] = Field(default_factory=datetime.now)
    updated_at: Optional[datetime] = None
    deleted_at: Optional[datetime] = None
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        json_schema_extra={
            "example": {
                "_id": None,
                "sender_id": "607f1f77bcf86cd799439011",
                "group_id": "607f1f77bcf86cd799439022",
                "content": "This is a message content example.",
                "created_at": "2023-10-01T12:34:56",
                "updated_at": None,
                "deleted_at": None,
            }
        },
    )


class CreateMessageRequestDto(BaseModel):
    group_id: str = Field(...)
    content: str = Field(...)

from typing import Annotated

from fastapi import APIRouter, status, HTTPException, Depends
from fastapi.security import OAuth2PasswordRequestForm
from argon2.exceptions import VerificationError, VerifyMismatchError, InvalidHashError

from app.db.models.auth import Token
from app.exceptions.auth import EmailAlreadyExists, InvalidDataInToken
from app.exceptions.user import UserNotFoundError
from app.handlers.commands.user.create_user import CreateUserCommand
from app.handlers.queries.login import LoginUserQuery
from app.mediator.mediator import mediator

router = APIRouter()


@router.post(
    "/login", status_code=status.HTTP_200_OK, description="Login for JWT token"
)
async def get_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
) -> Token:
    """
    Endpoint to log in a user and return an access token.

    :type form_data: user credentials

    :return: A valid access token if login is successful.
    :rtype: dict

    :raises HTTPException: If the user is not found or any other error occurs during login.
    """
    try:
        return await mediator.send_async(
            LoginUserQuery(email=form_data.username, password=form_data.password)
        )
    except (
        UserNotFoundError,
        InvalidDataInToken,
        InvalidHashError,
        VerificationError,
        VerifyMismatchError,
    ) as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=str(e),
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Internal server error",
        )


@router.post(
    "/register",
    description="Endpoint for creating users",
    status_code=status.HTTP_201_CREATED,
)
async def create_user(user_data: CreateUserCommand):
    """
    Endpoint to create a new user.

    :param user_data: The user data to be processed.
    :type user_data: CreateUserCommand

    :raises HTTPException: If any error occurs during user creation.
    """
    try:
        return await mediator.send_async(user_data)
    except EmailAlreadyExists as e:
        raise HTTPException(status_code=409, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

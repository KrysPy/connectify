from fastapi import APIRouter, HTTPException, status

from app.db.models.message import CreateMessageRequestDto
from app.config.dependencies import user_dependency
from app.exceptions.group import GroupNotFoundError
from app.handlers.commands.message.create_message import CreateMessageCommand
from app.handlers.queries.get_message import GetMessagesForGroupQuery
from app.mediator.mediator import mediator

router = APIRouter()


@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    description="Endpoint to create message",
)
async def create_message(user: user_dependency, message_data: CreateMessageRequestDto):
    """
    Endpoint to create message.

    :param user: injected current authenticated user
    :param message_data: data required to create new message
    :type message_data: CreateMessageRequestDto
    """
    try:
        return await mediator.send_async(
            CreateMessageCommand(
                sender_id=user.user_id,
                group_id=message_data.group_id,
                content=message_data.content,
            )
        )
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")


@router.get(
    "/group/{group_id}",
    status_code=status.HTTP_200_OK,
    description="Endpoint to getting all messages for group",
)
async def get_messages_for_group(user: user_dependency, group_id: str):
    """
    Endpoint to create message.

    :param user: injected current authenticated user
    :param group_id: ID of a group to get messages
    :type group_id: str
    """
    try:
        return await mediator.send_async(
            GetMessagesForGroupQuery(group_id=group_id, user_id=user.user_id)
        )
    except GroupNotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")

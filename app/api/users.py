from typing import Annotated
import logging

from fastapi import APIRouter, HTTPException, status
from pydantic.functional_validators import BeforeValidator

from app.config.dependencies import user_dependency
from app.exceptions.user import UserNotFoundError
from app.handlers.queries.get_user import GetUserQuery
from app.mediator.mediator import mediator

router = APIRouter()
log = logging.getLogger(__name__)
PyObjectId = Annotated[str, BeforeValidator(str)]


@router.get(
    "/{user_id}",
    status_code=status.HTTP_200_OK,
    description="Endpoint for getting one user by id",
)
async def get_user(user: user_dependency, user_id: str):
    """
    Endpoint to retrieve a user by their unique ID.

    :param user: injected current authenticated user
    :param user_id: The unique ID of the user to retrieve.
    :type user_id: str

    :returns: User details in JSON format if the user is found.
    :rtype: JSON

    :raises HTTPException:
        - 404: If the user wi does not exist.
        - 500: If an unexpected error occurs during processing.
    """
    if user is None:
        raise HTTPException(status_code=404, detail="User does not exist")

    if user_id == user.user_id:
        try:
            return await mediator.send_async(GetUserQuery(user_id=user_id))
        except UserNotFoundError as e:
            raise HTTPException(status_code=404, detail=str(e))
        except Exception as e:
            raise HTTPException(
                status_code=500, detail=f"Internal server error: {str(e)}"
            )
    raise HTTPException(status_code=401, detail="Unauthorized")

from httpx import AsyncClient, ASGITransport
import pytest

from app.main import app


@pytest.mark.asyncio
async def test_get_user_by_id_authorized(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        user_id = res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        get_res = await client.get(f"/user/{user_id}", headers=headers)

    assert get_res.status_code == 200
    assert get_res.json()["email"] == create_user_payload["email"]
    assert get_res.json()["username"] == create_user_payload["username"]


@pytest.mark.asyncio
async def test_get_user_by_id_unauthorized(mock_db, create_user_payload):
    sec_user = {
        "email": "user2@example.com",
        "username": "testuser",
        "password": "securepassword123",
        "full_name": "Test User",
    }

    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        await client.post("/auth/register", json=create_user_payload)
        sec_user_res = await client.post("/auth/register", json=sec_user)

        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        sec_user_id = sec_user_res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        get_res = await client.get(f"/user/{sec_user_id}", headers=headers)

    assert get_res.status_code == 401
    assert get_res.json()["detail"] == "Unauthorized"

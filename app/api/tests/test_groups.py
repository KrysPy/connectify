from bson import ObjectId

from httpx import AsyncClient, ASGITransport
import pytest

from app.main import app


@pytest.mark.asyncio
async def test_create_group_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        user_id = res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        group_payload = {"name": "Sample group", "is_private": False}

        group_res = await client.post("/group/", json=group_payload, headers=headers)

        assert group_res.status_code == 201

        created_group = await mock_db.groups.find_one({"name": "Sample group"})

        assert created_group
        assert created_group["owner_id"] == user_id
        assert created_group["is_private"] == False


@pytest.mark.asyncio
async def test_get_all_user_groups_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        user_id = res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        sample_groups = [
            {"name": "Group1", "owner_id": user_id, "is_private": False},
            {"name": "Group2", "owner_id": user_id, "is_private": True},
        ]
        await mock_db.groups.insert_many(sample_groups)

        groups_res = await client.get("/group/", headers=headers)

        assert groups_res.status_code == 200
        groups = groups_res.json()
        assert len(groups) == 2
        assert groups[0]["owner_id"] == user_id
        assert groups[1]["owner_id"] == user_id


@pytest.mark.asyncio
async def test_get_group_by_id_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        user_id = res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        group_payload = {
            "name": "Sample group",
            "owner_id": user_id,
            "is_private": False,
        }
        insert_res = await mock_db.groups.insert_one(group_payload)
        group_id = str(insert_res.inserted_id)

        group_res = await client.get(f"/group/{group_id}", headers=headers)

        assert group_res.status_code == 200
        group = group_res.json()
        assert group["name"] == "Sample group"
        assert group["owner_id"] == user_id
        assert group["is_private"] == False


@pytest.mark.asyncio
async def test_get_group_by_id_not_found(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        non_existent_group_id = "64fa1234a5e123abc1234567"
        group_res = await client.get(f"/group/{non_existent_group_id}", headers=headers)

        assert group_res.status_code == 404


@pytest.mark.asyncio
async def test_update_group_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        user_id = res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        group_payload = {"name": "Sample group", "is_private": False}
        group_res = await client.post("/group/", json=group_payload, headers=headers)
        assert group_res.status_code == 201

        group_id = group_res.json()["group_id"]

        update_payload = {"name": "Updated Group Name", "is_private": True}
        update_res = await client.put(
            f"/group/{group_id}", json=update_payload, headers=headers
        )

        assert update_res.status_code == 200
        updated_group = update_res.json()

        assert updated_group["name"] == "Updated Group Name"
        assert updated_group["is_private"] == True
        assert updated_group["owner_id"] == user_id

        db_group = await mock_db.groups.find_one({"_id": ObjectId(group_id)})
        assert db_group["name"] == "Updated Group Name"
        assert db_group["is_private"] == True


@pytest.mark.asyncio
async def test_delete_group_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        group_payload = {"name": "Group to Delete", "is_private": False}
        group_res = await client.post("/group/", json=group_payload, headers=headers)
        assert group_res.status_code == 201

        group_id = group_res.json()["group_id"]

        delete_res = await client.delete(f"/group/{group_id}", headers=headers)

        assert delete_res.status_code == 200
        delete_response = delete_res.json()
        assert delete_response["message"] == "Group deleted successfully"
        assert delete_response["group_id"] == group_id

        deleted_group = await mock_db.groups.find_one({"_id": ObjectId(group_id)})
        assert deleted_group is None

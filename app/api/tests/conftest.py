import pytest

from mongomock_motor import AsyncMongoMockClient

from app.db.db import Database


@pytest.fixture
def mock_db():
    mock_client = AsyncMongoMockClient()
    mock_db = mock_client.get_database("connectify")

    Database.client = mock_client

    return mock_db


@pytest.fixture
def create_user_payload():
    return {
        "email": "user@example.com",
        "username": "testuser",
        "password": "securepassword123",
        "full_name": "Test User",
    }

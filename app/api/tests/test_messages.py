from httpx import AsyncClient, ASGITransport
import pytest

from app.main import app


@pytest.mark.asyncio
async def test_create_message_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        register_res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        user_id = register_res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        group_payload = {"name": "Sample group", "is_private": False}
        group_res = await client.post("/group/", json=group_payload, headers=headers)
        assert group_res.status_code == 201
        group_id = group_res.json()["group_id"]

        message_payload = {"group_id": group_id, "content": "Test message content"}
        message_res = await client.post(
            "/messages/", json=message_payload, headers=headers
        )

        assert message_res.status_code == 201
        created_message = await mock_db.messages.find_one(
            {"content": "Test message content"}
        )
        assert created_message
        assert created_message["sender_id"] == user_id
        assert created_message["group_id"] == group_id
        assert created_message["content"] == "Test message content"


@pytest.mark.asyncio
async def test_get_messages_for_group_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        register_res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )
        assert login_res.status_code == 200

        user_id = register_res.json()["user_id"]
        token = login_res.json()["access_token"]
        headers = {"Authorization": f"Bearer {token}"}

        group_payload = {"name": "Test Group", "is_private": False}
        group_res = await client.post("/group/", json=group_payload, headers=headers)
        assert group_res.status_code == 201
        group_id = group_res.json()["group_id"]

        messages = [
            {"group_id": group_id, "content": "Hello, Group!"},
            {"group_id": group_id, "content": "Welcome everyone!"},
        ]
        for message in messages:
            await mock_db.messages.insert_one(
                {
                    "sender_id": user_id,
                    "group_id": group_id,
                    "content": message["content"],
                }
            )

        response = await client.get(f"/messages/group/{group_id}", headers=headers)

        assert response.status_code == 200
        fetched_messages = response.json()
        assert len(fetched_messages) == 2
        assert fetched_messages[0]["content"] == "Hello, Group!"
        assert fetched_messages[1]["content"] == "Welcome everyone!"

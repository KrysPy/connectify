from httpx import AsyncClient, ASGITransport
from pymongo.errors import PyMongoError
import pytest

from app.main import app


@pytest.mark.asyncio
async def test_create_user_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)

    assert res.status_code == 201

    created_user = await mock_db.users.find_one({"email": "user@example.com"})
    assert created_user is not None
    assert created_user["email"] == "user@example.com"
    assert created_user["full_name"] == "Test User"


@pytest.mark.asyncio
async def test_create_user_duplicated_email(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        res_duplicate = await client.post("/auth/register", json=create_user_payload)

    assert res.status_code == 201
    assert res_duplicate.status_code == 409


@pytest.mark.asyncio
async def test_login_success(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )

    assert res.status_code == 201
    assert login_res.status_code == 200
    assert "access_token" in login_res.json()
    assert login_res.json()["token_type"] == "bearer"


@pytest.mark.asyncio
async def test_login_failed_invalid_password(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": "failingpassword",
            },
        )

    assert res.status_code == 201
    assert login_res.status_code == 401


@pytest.mark.asyncio
async def test_login_success_invalid_email(mock_db, create_user_payload):
    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        res = await client.post("/auth/register", json=create_user_payload)
        login_res = await client.post(
            "/auth/login",
            data={
                "username": "invalidemail@example.com",
                "password": create_user_payload["password"],
            },
        )

    assert res.status_code == 201
    assert login_res.status_code == 401


@pytest.mark.asyncio
async def test_login_unhandled_error_mongo(mocker, create_user_payload):
    mocker.patch(
        "app.mediator.mediator.mediator.send_async",
        side_effect=PyMongoError("Unexpected database error"),
    )

    async with AsyncClient(
        transport=ASGITransport(app), base_url="http://testserver"
    ) as client:
        login_res = await client.post(
            "/auth/login",
            data={
                "username": create_user_payload["email"],
                "password": create_user_payload["password"],
            },
        )

    assert login_res.status_code == 500
    assert login_res.json()["detail"] == "Internal server error"

from fastapi import APIRouter, HTTPException, status

from app.db.models.group import UpdateGroupRequestDto
from app.exceptions.group import GroupNotFoundError, PermissionDeniedError
from app.config.dependencies import user_dependency
from app.handlers.commands.group.create_group import CreateGroupCommand
from app.handlers.commands.group.delete_group import DeleteGroupCommand
from app.handlers.commands.group.edit_group import UpdateGroupCommand
from app.handlers.queries.get_group import GetAllUserGroupsQuery, GetGroupByIdQuery
from app.mediator.mediator import mediator

router = APIRouter()


@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    description="Endpoint to create group channel for users",
)
async def create_group(user: user_dependency, group_data: CreateGroupCommand):
    """
    Endpoint to create group channel for users.

    :param user: injected current authenticated user
    :param group_data: data required to create new team group
    :type group_data: CreateGroupCommand
    """
    try:
        group_data.owner_id = user.user_id
        return await mediator.send_async(group_data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")


@router.get("/", status_code=status.HTTP_200_OK, description="Get all groups")
async def get_all_user_groups(user: user_dependency):
    """
    Endpoint to retrieve all groups owned by the current authenticated user.

    :param user: injected current authenticated user
    :type user: user_dependency
    :return: list of groups owned by the user
    :rtype: list[dict]
    """
    try:
        return await mediator.send_async(GetAllUserGroupsQuery(owner_id=user.user_id))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")


@router.get(
    "/{group_id}", status_code=status.HTTP_200_OK, description="Get group by ID"
)
async def get_group_by_id(user: user_dependency, group_id: str):
    """
    Endpoint to retrieve a specific group by its ID.

    :param user: injected current authenticated user
    :type user: user_dependency
    :param group_id: unique identifier of the group to retrieve
    :type group_id: str
    :return: group details if found
    :rtype: dict
    :raises HTTPException: 404 error if group not found, 500 error for internal server errors
    """
    try:
        return await mediator.send_async(
            GetGroupByIdQuery(group_id=group_id, owner_id=user.user_id)
        )
    except PermissionDeniedError as e:
        raise HTTPException(status_code=403, detail=str(e))
    except GroupNotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")


@router.put(
    "/{group_id}",
    status_code=status.HTTP_200_OK,
    description="Endpoint to update group details",
)
async def update_group(
    user: user_dependency, group_id: str, update_data: UpdateGroupRequestDto
):
    """
    Endpoint to update details of a specific group by its ID.

    :param user: injected current authenticated user
    :param group_id: unique identifier of the group to update
    :type group_id: str
    :param update_data: data required to update the group (name, privacy setting)
    :type update_data: UpdateGroupCommand
    :return: updated group details
    :rtype: dict
    :raises HTTPException: 404 error if group not found, 500 error for internal server errors
    """
    try:
        return await mediator.send_async(
            UpdateGroupCommand(
                group_id=group_id,
                owner_id=user.user_id,
                name=update_data.name,
                is_private=update_data.is_private,
            )
        )
    except GroupNotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except PermissionDeniedError as e:
        raise HTTPException(status_code=403, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")


@router.delete(
    "/{group_id}",
    status_code=status.HTTP_200_OK,
    description="Endpoint to delete a specific group",
)
async def delete_group(user: user_dependency, group_id: str):
    """
    Endpoint to delete a specific group by its ID.

    :param user: injected current authenticated user
    :param group_id: unique identifier of the group to delete
    :type group_id: str
    :return: success message and ID of deleted group
    :rtype: dict
    :raises HTTPException: 404 error if group not found, 500 error for internal server errors
    """
    try:
        return await mediator.send_async(
            DeleteGroupCommand(group_id=group_id, owner_id=user.user_id)
        )
    except GroupNotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")

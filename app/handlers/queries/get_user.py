from pydantic import BaseModel

from app.service.user_service import UserService

user_service = UserService()


class GetUserQuery(BaseModel):
    """Get user query object"""

    user_id: str


class GetUserHandler:
    async def handle(self, request: GetUserQuery) -> dict:
        """
        Handle GetUserQuery request to fetch user details.

        :param request: The query object containing the user ID.

        :returns: A dictionary containing user details if found.

        """
        return await user_service.get_user_by_id(request.user_id)

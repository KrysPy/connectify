from pydantic import BaseModel

from app.service.group_service import GroupService

group_service = GroupService()


class GetAllUserGroupsQuery(BaseModel):
    """Query object to retrieve all groups for a specific user."""

    owner_id: str


class GetGroupByIdQuery(BaseModel):
    """Query object to retrieve a specific group by its ID."""

    group_id: str
    owner_id: str


class GetAllUserGroupsHandler:
    async def handle(self, request: GetAllUserGroupsQuery) -> list[dict]:
        """
        Handle GetAllUserGroupsQuery request to fetch all groups for a user.

        :param request: The query object containing the user ID (owner_id).

        :returns: A list of dictionaries, each representing a group owned by the user.
        """
        return await group_service.get_all_user_groups(request.owner_id)


class GetGroupByIdHandler:
    async def handle(self, request: GetGroupByIdQuery) -> dict:
        """
        Handle GetGroupByIdQuery request to fetch details of a specific group.

        :param request: The query object containing the group ID.

        :returns: A dictionary containing details of the specified group.
        """
        return await group_service.get_group_by_id(request.group_id, request.owner_id)

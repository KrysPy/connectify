from pydantic import BaseModel, Field

from app.exceptions.user import UserNotFoundError
from app.service.auth_service import authenticate_user, create_access_token
from app.db.models.auth import Token


class LoginUserQuery(BaseModel):
    """Login user query object"""

    email: str = Field(...)
    password: str = Field(...)


class LoginUserHandler:
    async def handle(self, request: LoginUserQuery) -> Token:
        """
        Handles the user login process by authenticating the user and generating an access token.

        :param request: The login request containing the user's email and password.
        :type request: LoginUserQuery

        :return: A JWT access token if authentication is successful.
        :rtype: Token

        :raises UserNotFoundError: If the user is not found or authentication fails.
        """
        user = await authenticate_user(request.email, request.password)
        if user:
            access_token = create_access_token(
                {"sub": request.email, "id": user["_id"]}
            )
            return Token(access_token=access_token, token_type="bearer")
        raise UserNotFoundError

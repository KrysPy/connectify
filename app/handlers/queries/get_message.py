from pydantic import BaseModel

from app.service.message_service import MessageService

message_service = MessageService()


class GetMessagesForGroupQuery(BaseModel):
    """Query object to retrieve all messages for specific group."""

    group_id: str
    user_id: str


class GetMessagesForGroupHandler:
    async def handle(self, request: GetMessagesForGroupQuery) -> list[dict]:
        """
        Handle GetMessagesForGroupQuery request to fetch all messages for specific group.

        :param request: The query object containing the user ID (owner_id).

        :returns: A list of dictionaries, each representing a message in specific group.

        """
        return await message_service.get_message_for_group(
            request.group_id, request.user_id
        )

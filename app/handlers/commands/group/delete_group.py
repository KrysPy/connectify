from pydantic import BaseModel

from app.service.group_service import GroupService

group_service = GroupService()


class DeleteGroupCommand(BaseModel):
    """Delete group command object"""

    group_id: str
    owner_id: str


class DeleteGroupHandler:
    async def handle(self, request: DeleteGroupCommand):
        """
        Delete a group based on the request data.

        :param request: The request data for deleting a group.

        :return: The created group ID.
        """
        return await group_service.delete_group(request.group_id, request.owner_id)

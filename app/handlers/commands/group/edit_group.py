from pydantic import BaseModel, Field

from app.service.group_service import GroupService

group_service = GroupService()


class UpdateGroupCommand(BaseModel):
    """Update group command object"""

    group_id: str = Field(...)
    owner_id: str = Field(...)
    name: str = Field(...)
    is_private: bool = Field(default=False)


class UpdateGroupHandler:
    async def handle(self, request: UpdateGroupCommand):
        """
        Edit a group based on the request data.

        :param request: The request data for editing a new group.

        :return: The created group ID.
        """
        return group_service.update_group(
            request.group_id, request.owner_id, request.name, request.is_private
        )

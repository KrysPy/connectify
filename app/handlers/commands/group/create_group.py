from typing import Optional

from pydantic import BaseModel, Field

from app.service.group_service import GroupService

group_service = GroupService()


class CreateGroupCommand(BaseModel):
    """Create group command object"""

    name: str = Field(...)
    is_private: bool = Field(default=False)
    owner_id: Optional[str] = None


class CreateGroupHandler:
    async def handle(self, request: CreateGroupCommand):
        """
        Create a new group based on the request data.

        :param request: The request data for creating a new group.

        :return: The created group ID.
        """
        return await group_service.create_group(
            request.name, request.owner_id, request.is_private
        )

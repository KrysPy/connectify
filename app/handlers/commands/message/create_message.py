from pydantic import BaseModel

from app.service.message_service import MessageService

message_service = MessageService()


class CreateMessageCommand(BaseModel):
    """Create message command object"""

    sender_id: str
    group_id: str
    content: str


class CreateMessageHandler:
    async def handle(self, request: CreateMessageCommand):
        """
        Create a new message based on the request data.

        :param request: The request data for creating a new message.

        :return: The created message ID.
        """
        return message_service.create_message(
            request.sender_id, request.group_id, request.content
        )

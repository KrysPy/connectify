from pydantic import BaseModel, EmailStr, Field
from argon2 import PasswordHasher

from app.service.user_service import UserService

user_service = UserService()


class CreateUserCommand(BaseModel):
    """Create user command object"""

    username: str = Field(...)
    email: EmailStr = Field(...)
    password: str = Field(...)
    full_name: str = Field(...)


class CreateUserHandler:
    def handle(self, request: CreateUserCommand):
        """
        Create a new user based on the request data.

        :param request: The request data for creating a new user.

        :return: The created user ID.
        """
        hashed_password = PasswordHasher().hash(request.password)

        return user_service.create_user(
            request.username, request.email, hashed_password, request.full_name
        )

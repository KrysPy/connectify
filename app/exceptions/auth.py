class InvalidDataInToken(Exception):
    """
    Exception raised when a data in token is invalid.
    User data not found in token
    """

    def __init__(self, message="Invalid data in token"):
        self.message = message
        super().__init__(self.message)


class EmailAlreadyExists(Exception):
    def __init__(self, message="User with such email already exist"):
        self.message = message
        super().__init__(self.message)

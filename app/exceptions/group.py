class GroupAlreadyExists(Exception):
    def __init__(self, message="User already has a group with that name"):
        self.message = message
        super().__init__(self.message)


class GroupNotFoundError(Exception):
    def __init__(self, message="Group with given ID not found"):
        self.message = message
        super().__init__(self.message)


class PermissionDeniedError(Exception):
    def __init__(self, message="Permission denied"):
        self.message = message
        super().__init__(self.message)
